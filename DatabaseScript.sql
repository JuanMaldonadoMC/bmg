USE [master]
GO
/****** Object:  Database [BMGProject]    Script Date: 11/19/2020 15:14:32 ******/
IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = N'BMGProject')
BEGIN
CREATE DATABASE [BMGProject] ON  PRIMARY 
( NAME = N'BMGProject', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.DEVSERVER\MSSQL\DATA\BMGProject.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BMGProject_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.DEVSERVER\MSSQL\DATA\BMGProject_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
END
GO
ALTER DATABASE [BMGProject] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BMGProject].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BMGProject] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [BMGProject] SET ANSI_NULLS OFF
GO
ALTER DATABASE [BMGProject] SET ANSI_PADDING OFF
GO
ALTER DATABASE [BMGProject] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [BMGProject] SET ARITHABORT OFF
GO
ALTER DATABASE [BMGProject] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [BMGProject] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [BMGProject] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [BMGProject] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [BMGProject] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [BMGProject] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [BMGProject] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [BMGProject] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [BMGProject] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [BMGProject] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [BMGProject] SET  DISABLE_BROKER
GO
ALTER DATABASE [BMGProject] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [BMGProject] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [BMGProject] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [BMGProject] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [BMGProject] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [BMGProject] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [BMGProject] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [BMGProject] SET  READ_WRITE
GO
ALTER DATABASE [BMGProject] SET RECOVERY FULL
GO
ALTER DATABASE [BMGProject] SET  MULTI_USER
GO
ALTER DATABASE [BMGProject] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [BMGProject] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'BMGProject', N'ON'
GO
USE [BMGProject]
GO
/****** Object:  Table [dbo].[Issuer]    Script Date: 11/19/2020 15:14:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Issuer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Issuer](
	[issuer_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](250) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[issuer_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[account]    Script Date: 11/19/2020 15:14:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[account]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[account](
	[account_id] [int] IDENTITY(1,1) NOT NULL,
	[cash] [float] NOT NULL,
	[stock] [float] NOT NULL,
 CONSTRAINT [PK_account] PRIMARY KEY CLUSTERED 
(
	[account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Movement]    Script Date: 11/19/2020 15:14:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Movement]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Movement](
	[movement_id] [int] IDENTITY(1,1) NOT NULL,
	[account_id] [int] NOT NULL,
	[movement_type] [varchar](4) NOT NULL,
	[storage_date] [datetime] NOT NULL,
	[request_date] [datetime] NOT NULL,
	[issuer_id] [int] NOT NULL,
	[share_price] [float] NOT NULL,
	[total_shares] [float] NOT NULL,
	[was_applied] [bit] NOT NULL,
	[error_code] [int] NULL,
 CONSTRAINT [PK_Movement] PRIMARY KEY CLUSTERED 
(
	[movement_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Default [DF_Movement_storage_date]    Script Date: 11/19/2020 15:14:33 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Movement_storage_date]') AND parent_object_id = OBJECT_ID(N'[dbo].[Movement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Movement_storage_date]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Movement] ADD  CONSTRAINT [DF_Movement_storage_date]  DEFAULT (getdate()) FOR [storage_date]
END


End
GO
/****** Object:  ForeignKey [FK_Movement_account]    Script Date: 11/19/2020 15:14:33 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Movement_account]') AND parent_object_id = OBJECT_ID(N'[dbo].[Movement]'))
ALTER TABLE [dbo].[Movement]  WITH CHECK ADD  CONSTRAINT [FK_Movement_account] FOREIGN KEY([account_id])
REFERENCES [dbo].[account] ([account_id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Movement_account]') AND parent_object_id = OBJECT_ID(N'[dbo].[Movement]'))
ALTER TABLE [dbo].[Movement] CHECK CONSTRAINT [FK_Movement_account]
GO
/****** Object:  ForeignKey [FK_Movement_User]    Script Date: 11/19/2020 15:14:33 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Movement_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[Movement]'))
ALTER TABLE [dbo].[Movement]  WITH CHECK ADD  CONSTRAINT [FK_Movement_User] FOREIGN KEY([issuer_id])
REFERENCES [dbo].[Issuer] ([issuer_id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Movement_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[Movement]'))
ALTER TABLE [dbo].[Movement] CHECK CONSTRAINT [FK_Movement_User]
GO
