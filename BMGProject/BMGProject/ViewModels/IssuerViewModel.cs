﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMGProject.ViewModels
{
    public class IssuerViewModel
    {
        public string IssuerName { get; set; }
        public double SharePrice { get; set; }
        public double TotalShare { get; set; }

        public IssuerViewModel()
        {
            this.IssuerName = String.Empty;
        }
    }
}
