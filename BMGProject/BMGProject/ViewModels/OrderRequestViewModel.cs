﻿using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BMGProject.ViewModels
{
    [DataContract()]
    public class OrderRequestViewModel
    {
        [DataMember(Name = "timestamp", IsRequired = true)]
        public long RequestDate { get; set; }
        [DataMember(Name = "operation", IsRequired = true)]
        public string MovementType { get; set; }
        [DataMember(Name = "issuer_name", IsRequired = true)]
        public string IssuerName { get; set; }
        [DataMember(Name = "share_price", IsRequired = true)]
        public double SharePrice { get; set; }
        [DataMember(Name = "total_shares", IsRequired = true)]
        public double TotalShare { get; set; }

        public OrderRequestViewModel()
        {
            this.MovementType = String.Empty;
            this.IssuerName = String.Empty;
        }
    }
}
