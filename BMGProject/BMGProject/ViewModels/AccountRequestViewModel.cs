﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Newtonsoft.Json.Schema;

namespace BMGProject.ViewModels
{
    [DataContract()]
    public class AccountRequestViewModel
    {
        [DataMember(Name ="cash",IsRequired = true)]
        public double Cash { get; set; }
    }
}
