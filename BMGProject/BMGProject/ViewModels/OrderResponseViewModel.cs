﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace BMGProject.ViewModels
{
    [DataContract()]
    public class OrderResponseViewModel
    {
        [DataMember(Name = "cash")]
        public double Cash { get; set; }
        [DataMember(Name = "issuers")]
        public List<IssuerViewModel> Issuers { get; set; }
        [DataMember(Name = "business_errors")]
        public List<string> BusinessErrors { get; set; }

        public OrderResponseViewModel()
        {
            this.Issuers = new List<IssuerViewModel>();
            this.BusinessErrors = new List<string>();
        }

    }
}
