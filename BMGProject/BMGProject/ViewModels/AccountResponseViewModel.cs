﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace BMGProject.ViewModels
{
    [DataContract()]
    public class AccountResponseViewModel
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }
        [DataMember(Name = "cash")]
        public double Cash { get; set; }
        [DataMember(Name = "issuers")]
        public List<IssuerViewModel> Issuers { get; set; }

        public AccountResponseViewModel()
        {
            Issuers = new List<IssuerViewModel>();
        }

    }
}
