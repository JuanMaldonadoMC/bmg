﻿using BMGProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMGProject.Controllers.BusinessRules
{
    /// <summary>
    /// This class was created for be a base class of all validation Controller, 
    /// with the purpose that all validator have the same behavior and obviously each one with the specific implementation 
    /// only with the inherite of this class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BRValidatorController<T>
    {
        protected BRValidatorResult Result = null;
        protected List<Action> Validations=null;

        public BRValidatorController()
        {
            this.Result = GetCorrectResult();
            this.Validations = new List<Action>();
        }

        public virtual BRValidatorResult ExecuteValidation()
        {
            foreach (Action Validation in this.Validations)
            {
                Validation.Invoke();
                if (!this.Result.ValidationWasApproved)
                    return Result;
            }

            return this.Result;
        }

        private BRValidatorResult GetCorrectResult()
        {
            return new BRValidatorResult();
        }

    }


    /// <summary>
    /// This class was created for contain the validation response process and in this way can reject investment movements
    /// </summary>
    public class BRValidatorResult
    {
        public bool ValidationWasApproved { get; set; }
        public int? ErrorCode { get; set; }

        public BRValidatorResult()
        {
            this.ValidationWasApproved = true;
            this.ErrorCode = null;
        }

        public BRValidatorResult(int ErrorCode)
        {
            this.ValidationWasApproved = false;
            this.ErrorCode = ErrorCode;
        }

    }
}
