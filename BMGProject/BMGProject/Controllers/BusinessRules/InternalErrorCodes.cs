﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMGProject.Controllers.BusinessRules
{
    /// <summary>
    /// This is a internal enum of errors. This enum was created with the propourse to identify the errors and 
    /// we will can answer in different way for every one
    /// </summary>
    public enum InternalErrorCodes
    {
        InsuficientBalance = 1,
        InsuficientStock = 2,
        DuplicatedOperation =3,
        CloseMarket = 4,
        InvalidOperation =5
    }

    public class InternalErrorNames
    {

        private static Dictionary<int, string> DictionryErrorCodes = new Dictionary<int, string>();

        /// <summary>
        /// This method was created for get a internal code name filtering by internal error code
        /// </summary>
        /// <param name="ErrorCode"></param>
        /// <returns>
        /// Return a string with the name of internal error code, if the error code exists in the dictionary, 
        /// on the contrary return the string "Unknown operation"
        /// </returns>
        public static string ByCode(int ErrorCode)
        {
            Dictionary<int, string> DictionryErrorCodes = GetErrorCodesDictonary();
            if (DictionryErrorCodes.Where(i => i.Key == ErrorCode).Any())
            {
                return DictionryErrorCodes.Where(i => i.Key == ErrorCode).FirstOrDefault().Value;
            }

            return "Unknown operation";
        }

        private static Dictionary<int, string> GetErrorCodesDictonary()
        {
            Dictionary<int, string> DictionryErrorCodes = new Dictionary<int, string>();

            DictionryErrorCodes.Add((int)InternalErrorCodes.InsuficientBalance, "Insufficient Balance");
            DictionryErrorCodes.Add((int)InternalErrorCodes.InsuficientStock, "Insufficient Stock");
            DictionryErrorCodes.Add((int)InternalErrorCodes.DuplicatedOperation, "Movement Duplicated");
            DictionryErrorCodes.Add((int)InternalErrorCodes.CloseMarket, "Close Market");
            DictionryErrorCodes.Add((int)InternalErrorCodes.InvalidOperation, "Invalid operation");

            return DictionryErrorCodes;
        }
    }
}
