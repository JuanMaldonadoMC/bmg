﻿using BMGProject.Controllers.DBControllers;
using BMGProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMGProject.Controllers.BusinessRules
{
    public class MovimentValidatorController : BRValidatorController<MovementModel>
    {
        private MovementDBController MovementController { get; set; }
        private MovementModel MovementEntity { get; set; }
        private AccountModel InvestmentAccount { get; set; }
        

        public MovimentValidatorController(AccountModel AccountEntity,MovementDBController MovementController, MovementModel Entity) : base()
        {
            this.MovementController = MovementController;
            this.MovementEntity = Entity;
            this.InvestmentAccount = AccountEntity;

            this.Validations.Add(() => { ValidateMovementType(); });
            this.Validations.Add(() => { ValidateCloseMarket(); });
            this.Validations.Add(() => { ValidateBalance(); });
            this.Validations.Add(() => { ValidateStock(); });
            this.Validations.Add(() => { ValidateDuplicates(); });
        }

        private void ValidateMovementType()
        {
            if (!this.MovementEntity.MovementType.Equals(MovementModel.Type.BUY))
            {
                if (!this.MovementEntity.MovementType.Equals(MovementModel.Type.SELL))
                {
                    this.Result = new BRValidatorResult((int)InternalErrorCodes.InvalidOperation);
                }
            }
        }

        private void ValidateBalance()
        {
            if (this.MovementEntity.MovementType.Equals(MovementModel.Type.BUY))
            {
                double MovementAmount = this.MovementEntity.SharePrice * this.MovementEntity.TotalShare;
                if (this.InvestmentAccount.Cash < MovementAmount)
                {
                    this.Result = new BRValidatorResult((int)InternalErrorCodes.InsuficientBalance);
                }
            }
        }

        private void ValidateStock()
        {
            if (this.MovementEntity.MovementType.Equals(MovementModel.Type.SELL))
            {
                if (this.InvestmentAccount.Stock < this.MovementEntity.TotalShare)
                {
                    this.Result = new BRValidatorResult((int)InternalErrorCodes.InsuficientStock);
                }
            }
        }
    
        private void ValidateDuplicates()
        {
            if (this.MovementController.GetDBContext().Movement.
                                                Where(i => i.TotalShare == this.MovementEntity.TotalShare &&
                                                           i.SharePrice == this.MovementEntity.SharePrice &&
                                                           i.MovementType == this.MovementEntity.MovementType &&
                                                           i.StoregeDate >= DateTime.Now.AddMinutes(-5)).
                                                OrderByDescending(i => i.StoregeDate).Any())
            {
                this.Result = new BRValidatorResult((int)InternalErrorCodes.DuplicatedOperation);
            }
        }

        private void ValidateCloseMarket()
        {
            DateTime ServerDateTime = this.MovementController.GetDBContext().GetServerDateTime();
            Int32.TryParse(Environment.GetEnvironmentVariable(ListOfVariablesName.HOUR_CLOSE_MARKET), out int HourCloseMarket);
            Int32.TryParse(Environment.GetEnvironmentVariable(ListOfVariablesName.HOUR_OPEN_MARKET), out int HourOpenMarket);

            if (
                ServerDateTime >= (new DateTime(ServerDateTime.Year, ServerDateTime.Month, ServerDateTime.Day, HourCloseMarket,0,0)) 
                && 
                ServerDateTime <= (new DateTime(ServerDateTime.Year, ServerDateTime.Month, ServerDateTime.Day, HourOpenMarket, 0, 0))
                )
            {
                this.Result = new BRValidatorResult((int)InternalErrorCodes.CloseMarket);
            }
        }



    }
}
