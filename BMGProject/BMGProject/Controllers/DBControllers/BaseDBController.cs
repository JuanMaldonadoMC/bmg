﻿using BMGProject.Data;
using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace BMGProject.Controllers.DBControllers
{
    public class BaseDBController<T> : IDBControllers<T>, IDisposable
    {
        private readonly BMGProjectContext DBContext;
        private readonly bool DBAutoSave;

        public BaseDBController(BMGProjectContext Context, bool AutoSaveFlag)
        {
            this.DBContext = Context;
            this.DBAutoSave = AutoSaveFlag;
        }

        /// <summary>
        /// This method allow have access to the DBContext injected to the class
        /// </summary>
        /// <returns>
        /// Return BMGProjectContext Object that was injected in the class constructor
        /// </returns>
        public BMGProjectContext GetDBContext()
        {
            return this.DBContext;
        }

        /// <summary>
        /// This method allow have access to the AutoSave flag injected to the class
        /// </summary>
        /// <returns>
        /// Return bool flag that was injected in the class constructor
        /// </returns>
        public bool GetAutoSaveFlag()
        {
            return this.DBAutoSave;
        }

        /// <summary>
        /// This method allow send all changes to the database, only if the AutoSave flag injected is true
        /// </summary>
        protected void SaveChanges()
        {
            if (this.DBAutoSave)
            {
                this.DBContext.SaveChanges();
            }
        }

        public async virtual Task<IEnumerable<T>> AsyncGetAll()
        {
            throw new NotImplementedException();
        }

        public async virtual Task<T> AsyncGetEntity(int Id)
        {
            throw new NotImplementedException();
        }

        public virtual void Delete(int Id)
        {
            throw new NotImplementedException();
        }

        public virtual bool Exist(int Id)
        {
            throw new NotImplementedException();
        }

        public virtual T Save(T Entity)
        {
            throw new NotImplementedException();
        }

        public virtual T SaveOrUpdate(int Id,T Entity)
        {
            if (this.Exist(Id))
            {
                return this.Update(Entity);
            }
            else
            {
                return this.Save(Entity);
            }
        }

        public virtual T Update(T Entity)
        {
            throw new NotImplementedException();
        }



        #region Dispose process

        // To detect redundant calls
        private bool _disposed = false;

        // Instantiate a SafeHandle instance.
        private SafeHandle _safeHandle = new SafeFileHandle(IntPtr.Zero, true);

        // Protected implementation of Dispose pattern.
        protected void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                // Dispose managed state (managed objects).
                _safeHandle?.Dispose();
            }

            _disposed = true;

            // Call base class implementation.
            Dispose(disposing);
        }

        public void Dispose()
        {
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        #endregion

    }
}
