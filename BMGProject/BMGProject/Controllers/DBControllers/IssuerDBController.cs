﻿using BMGProject.Data;
using BMGProject.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMGProject.Controllers.DBControllers
{
    public class IssuerDBController : BaseDBController<IssuerModel>
    {
        public IssuerDBController(BMGProjectContext Context, bool AutoSaveFlag) : base(Context, AutoSaveFlag)
        {
        }

        override
        public bool Exist(int Id)
        {
            return this.GetDBContext().Issuer.Any(i => i.ID == Id);
        }

        /// <summary>
        /// This method execute a evaluation that the entity exist in the database filtering by the Name property
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>
        /// Return true if exist the entity with id filtered, and Return false when no found a entity with this value in Name property
        /// </returns>
        public bool Exist(string Name)
        {
            return this.GetDBContext().Issuer.Any(i => i.Name.Equals(Name));
        }

        override
        public IssuerModel Save(IssuerModel Entity)
        {
            this.GetDBContext().Add<IssuerModel>(Entity);
            this.SaveChanges();

            return Entity;
        }

        override
        public IssuerModel Update(IssuerModel Entity)
        {
            this.GetDBContext().Entry(Entity).State = EntityState.Modified;
            this.SaveChanges();

            return Entity;
        }

        override
        public async Task<IEnumerable<IssuerModel>> AsyncGetAll()
        {
            return await this.GetDBContext().Issuer.ToListAsync();
        }

        override
        public async Task<IssuerModel> AsyncGetEntity(int Id)
        {
            return await this.GetDBContext().Issuer.FindAsync(Id);
        }

        /// <summary>
        /// This method try to find an specific element filtering by the Name property
        /// </summary>
        /// <param name="Name"></param>
        /// <returns>
        /// Return the entity found or it can return null if the element not found in the database
        /// </returns>
        public IssuerModel GetEntityByName(string Name)
        {
            return this.GetDBContext().Issuer.Where(i=> i.Name.Equals(Name)).FirstOrDefault();
        }

    }
}
