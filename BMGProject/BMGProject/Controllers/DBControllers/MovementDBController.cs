﻿using BMGProject.Controllers.BusinessRules;
using BMGProject.Data;
using BMGProject.Models;
using BMGProject.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMGProject.Controllers.DBControllers
{
    public class MovementDBController : BaseDBController<MovementModel>
    {
        public MovementDBController(BMGProjectContext Context, bool AutoSaveFlag) : base(Context, AutoSaveFlag)
        {
        }

        override
        public bool Exist(int Id)
        {
            return this.GetDBContext().Movement.Any(i => i.ID == Id);
        }

        override
        public MovementModel Save(MovementModel Entity)
        {
            Entity.StoregeDate = this.GetDBContext().GetServerDateTime();
            this.GetDBContext().Add<MovementModel>(Entity);
            this.SaveChanges();

            return Entity;
        }

        override
        public MovementModel Update(MovementModel Entity)
        {
            Entity.StoregeDate = this.GetDBContext().GetServerDateTime();
            this.GetDBContext().Entry(Entity).State = EntityState.Modified;
            this.SaveChanges();

            return Entity;
        }

        override
        public async Task<IEnumerable<MovementModel>> AsyncGetAll()
        {
            return await (from mov in this.GetDBContext().Movement
                          join iss in this.GetDBContext().Issuer on mov.IssuerID equals iss.ID
                          select new MovementModel()
                          {
                              ID = mov.ID,
                              AccountID = mov.AccountID,
                              MovementType = mov.MovementType,
                              RequestDate = mov.RequestDate,
                              IssuerID = mov.IssuerID,
                              SharePrice = mov.SharePrice,
                              TotalShare = mov.TotalShare,
                              WasApplied = mov.WasApplied,
                              ErrorCode = mov.ErrorCode,
                              MovementIssuer = iss
                          }
                          ).ToListAsync();
        }

        override
        public async Task<MovementModel> AsyncGetEntity(int Id)
        {
            return await (from mov in this.GetDBContext().Movement
                          join iss in this.GetDBContext().Issuer on mov.IssuerID equals iss.ID
                          select new MovementModel()
                          {
                              ID = mov.ID,
                              AccountID = mov.AccountID,
                              MovementType = mov.MovementType,
                              RequestDate = mov.RequestDate,
                              IssuerID = mov.IssuerID,
                              SharePrice = mov.SharePrice,
                              TotalShare = mov.TotalShare,
                              WasApplied = mov.WasApplied,
                              ErrorCode = mov.ErrorCode,
                              MovementIssuer = iss
                          }
                          ).FirstAsync(i => i.ID == Id);
        }

        public async Task<AccountResponseViewModel> GetAllMovemetsByAccountAsync(AccountModel Account)
        {

            List<MovementModel> Movements = (List<MovementModel>)await this.AsyncGetAll();
            AccountResponseViewModel Response = new AccountResponseViewModel()
            {
                ID = Account.ID,
                Cash = Account.Cash,
                Issuers = (Movements.Where(m => m.AccountID == Account.ID && m.WasApplied == true).Select(m => new IssuerViewModel()
                {
                    IssuerName = m.MovementIssuer.Name,
                    SharePrice = m.SharePrice,
                    TotalShare = m.TotalShare
                }).ToList())
            };

            return Response;
        }

        public IEnumerable<IssuerViewModel> GetAllMovemets(int AccountID)
        {
            List<MovementModel> Movements = (from mov in this.GetDBContext().Movement
                    join iss in this.GetDBContext().Issuer on mov.IssuerID equals iss.ID
                    select new MovementModel()
                    {
                        ID = mov.ID,
                        AccountID = mov.AccountID,
                        MovementType = mov.MovementType,
                        RequestDate = mov.RequestDate,
                        IssuerID = mov.IssuerID,
                        SharePrice = mov.SharePrice,
                        TotalShare = mov.TotalShare,
                        WasApplied = mov.WasApplied,
                        ErrorCode = mov.ErrorCode,
                        MovementIssuer = iss
                    }
                    ).ToList();

            return (Movements.Where(m => m.AccountID == AccountID && m.WasApplied == true).Select(m => new IssuerViewModel()
                    {
                        IssuerName = m.MovementIssuer.Name,
                        SharePrice = m.SharePrice,
                        TotalShare = m.TotalShare
                    }).ToList());
        }


        /// <summary>
        /// This method is used for to do the orders process sended from request. This method encapsulates work needed for apply o reject
        /// the orders
        /// </summary>
        /// <param name="AccountID"></param>
        /// <param name="Request"></param>
        /// <returns>
        /// Return a response object with the investment account, Current cash and a list of issuers movements
        /// </returns>
        public OrderResponseViewModel ProcessMovements(int AccountID,OrderRequestViewModel Request)
        {
            OrderResponseViewModel Response = new OrderResponseViewModel();

            using (AccountDBController AccountController = new AccountDBController(this.GetDBContext(),this.GetAutoSaveFlag()))
            {
                using (MovementDBController MovementController = new MovementDBController(this.GetDBContext(), this.GetAutoSaveFlag()))
                {
                    using (IssuerDBController IssuerController = new IssuerDBController(this.GetDBContext(), this.GetAutoSaveFlag()))
                    {
                        try
                        {
                            AccountModel AccountEntity = AccountController.GetEntity(AccountID);
                            IssuerModel IssuerEntity = GetIssuerAfterProcessRequestItem(IssuerController, Request);
                            MovementModel MovementEntity = GetMovementAfterProcessRequestItem(MovementController, AccountEntity, IssuerEntity, Request);

                            if (MovementEntity.WasApplied)
                            {
                                AccountEntity = UpdateInvestmentAccount(AccountController, AccountEntity, Request);
                            }
                            else
                            {
                                Response.BusinessErrors.Add(InternalErrorNames.ByCode((int)MovementEntity.ErrorCode));
                            }

                            Response.Cash = AccountEntity.Cash;
                            Response.Issuers.AddRange(GetAllMovemets(AccountEntity.ID));

                            SaveChanges();
                        }
                        catch (System.Exception ex)
                        {
                            Response.BusinessErrors.Add(ex.Message);
                        }
                    }
                }
            }

            return Response;
        }

        private IssuerModel GetIssuerAfterProcessRequestItem(IssuerDBController IssuerController, OrderRequestViewModel Movement)
        {
            IssuerModel IssuerEntity = IssuerController.GetEntityByName(Movement.IssuerName);
            if (IssuerEntity == null)
            {
                IssuerEntity = new IssuerModel()
                {
                    Name = Movement.IssuerName
                };
                IssuerEntity = IssuerController.Save(IssuerEntity);
            }

            return IssuerEntity;
        }

        private MovementModel GetMovementAfterProcessRequestItem(MovementDBController MovementController, AccountModel AccountEntity, IssuerModel IssuerEntity, OrderRequestViewModel Movement)
        {
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime date = start.AddSeconds(Movement.RequestDate).ToLocalTime();

            MovementModel MovementEntity = new MovementModel()
            {
                AccountID = AccountEntity.ID,
                RequestDate = date,
                MovementType = Movement.MovementType,
                SharePrice = Movement.SharePrice,
                TotalShare = Movement.TotalShare,
                IssuerID = IssuerEntity.ID,
                MovementIssuer = IssuerEntity
            };

            BRValidatorResult ValidationResult = TheMovementIsValid(AccountEntity, MovementController, MovementEntity);
            MovementEntity.WasApplied = ValidationResult.ValidationWasApproved;
            MovementEntity.ErrorCode = ValidationResult.ErrorCode;

            MovementController.SaveOrUpdate(MovementEntity.ID,MovementEntity);

            return MovementEntity;
        }

        private BRValidatorResult TheMovementIsValid(AccountModel AccountEntity, MovementDBController MovementController,MovementModel MovementeEntity)
        {
            MovimentValidatorController Validator = new MovimentValidatorController(AccountEntity, MovementController,MovementeEntity);
            return Validator.ExecuteValidation();
        }

        private AccountModel UpdateInvestmentAccount(AccountDBController AccountController, AccountModel AccountEntity, OrderRequestViewModel Movement)
        {
            double TotalAmount = (Movement.TotalShare * Movement.SharePrice);

            if (Movement.MovementType.Equals(MovementModel.Type.SELL.ToString()))
            {
                AccountEntity.Cash += TotalAmount;
                AccountEntity.Stock -= Movement.TotalShare;
            }
            else
            {
                AccountEntity.Cash -= TotalAmount;
                AccountEntity.Stock += Movement.TotalShare;
            }

            AccountController.Update(AccountEntity);

            return AccountEntity;
        }

    }
}
