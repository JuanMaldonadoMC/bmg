﻿using BMGProject.Data;
using BMGProject.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMGProject.Controllers.DBControllers
{
    public class AccountDBController : BaseDBController<AccountModel>
    {
        public AccountDBController(BMGProjectContext Context, bool AutoSaveFlag) : base(Context, AutoSaveFlag)
        {
        }

        
        public override bool Exist(int Id)
        {
            return this.GetDBContext().Account.Any(i => i.ID == Id);
        }

        public override AccountModel Save(AccountModel Entity)
        {
            this.GetDBContext().Add<AccountModel>(Entity);
            this.SaveChanges();

            return Entity;
        }

        public override AccountModel Update(AccountModel Entity)
        {
            this.GetDBContext().Entry(Entity).State=EntityState.Modified;
            this.SaveChanges();

            return Entity;
        }

        public override async Task<IEnumerable<AccountModel>> AsyncGetAll()
        {
            return await this.GetDBContext().Account.ToListAsync();
        }

        public override async Task<AccountModel> AsyncGetEntity(int Id)
        {
            return await this.GetDBContext().Account.FindAsync(Id);
        }

        /// <summary>
        /// This method try to find an specific element filtering by the id property
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>
        /// Return the entity found or it can return null if the element not found in the database
        /// </returns>
        public AccountModel GetEntity(int Id)
        {
            return this.GetDBContext().Account.Find(Id);
        }


    }
}
