﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMGProject.Controllers.DBControllers
{
    public interface IDBControllers<T>
    {
        /// <summary>
        /// This method execute a evaluation that the entity exist in the database filtering by the id property
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>
        /// Return true if exist the entity with id filtered, and Return false when no found a entity with this value in id property
        /// </returns>
        bool Exist(int Id);

        /// <summary>
        /// This method add a new element for the entity
        /// </summary>
        /// <param name="Entity"></param>
        /// <returns>
        /// Return the same entity but with the correct Id value
        /// </returns>
        T Save(T Entity);

        /// <summary>
        /// This is a async method that try to find an specific element filtering by the id property
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>
        /// Return the entity found or it can return null if the element not found in the database
        /// </returns>
        Task<T> AsyncGetEntity(int Id);

        /// <summary>
        /// This is a async method that get all elements of entity en the database without some filter 
        /// </summary>
        /// <returns>
        /// Return a list of entities founds or it can return empty list if the entity don't have elements in the database
        /// </returns>
        Task<IEnumerable<T>> AsyncGetAll();

        /// <summary>
        /// This method update all the current change of the element in the database
        /// </summary>
        /// <param name="Entity"></param>
        /// <returns>
        /// Return the same entity but updated
        /// </returns>
        T Update(T Entity);

        /// <summary>
        /// This method have some logic for apply a insert action or update action  without the developer need verify 
        /// what action he need apply. Use the implementation of three more methods : Save , Update and Exist
        /// </summary>
        /// <param name="Entity"></param>
        /// <returns>
        /// If apply save action Return the same entity but with the correct Id value, 
        /// but apply update action Return the same entity but updated
        /// </returns>
        T SaveOrUpdate(int Id,T Entity);

        /// <summary>
        /// This method have a responsibility of remove elements from entity, this method find a elemnte filtering by the id property
        /// </summary>
        /// <param name="Id"></param>
        void Delete(int Id);

    }
}
