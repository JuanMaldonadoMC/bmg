﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BMGProject.Data;
using BMGProject.Models;
using BMGProject.ViewModels;
using Newtonsoft.Json;
using BMGProject.Controllers.DBControllers;
using System.IO;
using System.Transactions;

namespace BMGProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly BMGProjectContext _context;

        public AccountsController(BMGProjectContext context)
        {
            _context = context;
        }

        // GET: api/Accounts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AccountResponseViewModel>>> GetAccount()
        {
            bool AutoSave = false;
            AccountDBController AccountsController = new AccountDBController(this._context, AutoSave);
            MovementDBController MovementsController = new MovementDBController(this._context, AutoSave);

            List< AccountModel > Accounts = (List<AccountModel>)await AccountsController.AsyncGetAll();
            List<MovementModel> Movements = (List<MovementModel>)await MovementsController.AsyncGetAll();

            return Accounts.Select(a => new AccountResponseViewModel()
            {
                ID = a.ID,
                Cash = a.Cash,
                Issuers = (Movements.Where(m => m.AccountID == a.ID && m.WasApplied == true).Select(m => new IssuerViewModel()
                            {
                                IssuerName = m.MovementIssuer.Name,
                                SharePrice = m.SharePrice,
                                TotalShare = m.TotalShare
                            }).ToList())
            }).ToList();
        }

        // GET: api/Accounts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AccountResponseViewModel>> GetAccount(int id)
        {
            bool AutoSave = false;
            AccountDBController AccountsController = new AccountDBController(this._context, AutoSave);
            MovementDBController MovementsController = new MovementDBController(this._context, AutoSave);

            AccountModel Account = await AccountsController.AsyncGetEntity( id );
            if (Account == null)
            {
                return NotFound();
            }

            AccountResponseViewModel Response = await MovementsController.GetAllMovemetsByAccountAsync(Account);

            return Response;
        }

        // POST: api/Accounts/5/Orders
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost("{id}/Orders")]
        public async Task<ActionResult<OrderResponseViewModel>> PostOrdersToAccount(int Id)
        {
            using (TransactionScope Transaction =new TransactionScope())
            {
                try
                {
                    String Body = await GetBodyAsync();
                    OrderRequestViewModel RequestOrders = JsonConvert.DeserializeObject<OrderRequestViewModel>(Body);

                    if(RequestOrders == null)
                    {
                        return BadRequest();
                    }

                    bool AutoSave = true;
                    OrderResponseViewModel Response = new OrderResponseViewModel();

                    using (MovementDBController controller = new MovementDBController(this._context, AutoSave))
                    {
                        Response = controller.ProcessMovements(Id, RequestOrders);
                    }

                    Transaction.Complete();
                    return CreatedAtAction("GetAccount", new { id = Id }, Response);
                    
                }
                catch (Exception ex)
                {
                        
                    Transaction.Dispose();
                    return BadRequest(ex.Message);
                }
            }
        }

        // POST: api/Accounts
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<AccountResponseViewModel>> PostAccount()
        {
            using (TransactionScope Transaction = new TransactionScope())
            {
                try
                {
                    String Body = await GetBodyAsync();
                    AccountRequestViewModel RequestAccount = JsonConvert.DeserializeObject<AccountRequestViewModel>(Body);

                    bool AutoSave = true;
                    AccountModel account = new AccountModel(RequestAccount.Cash);
                    using (AccountDBController controller = new AccountDBController(this._context, AutoSave))
                    {
                        account = controller.SaveOrUpdate(account.ID, account);
                    }

                    AccountResponseViewModel response = new AccountResponseViewModel()
                    {
                        ID = account.ID,
                        Cash = account.Cash,
                    };

                    Transaction.Complete();
                    return CreatedAtAction("GetAccount", new { id = response.ID }, response);

                }
                catch (Exception)
                {
                    Transaction.Dispose();
                    return BadRequest();
                }
            }
        }

        // DELETE: api/Accounts/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AccountModel>> DeleteAccount(int id)
        {
            return NoContent();
        }

        private async Task<string> GetBodyAsync()
        {
            var stream = new StreamReader(Request.Body);
            return await stream.ReadToEndAsync();
        }

    }
}
