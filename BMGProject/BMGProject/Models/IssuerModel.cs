﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace BMGProject.Models
{
    [Table(name:"Issuer")]
    public class IssuerModel
    {
        [Column(name:"issuer_id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Column(name: "name")]
        public string Name { get; set; }

        public IssuerModel()
        {
            this.Name = String.Empty;
        }

    }
}
