﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace BMGProject.Models
{
    [Table(name:"Account")]
    public class AccountModel
    {
        [Column(name:"account_id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Column(name: "cash")]
        public double Cash { get; set; }
        [Column(name: "stock")]
        public double Stock { get; set; }

        public AccountModel()
        {
        }

        public AccountModel(double Cash)
        {
            this.Cash = Cash;
        }
    }
}
