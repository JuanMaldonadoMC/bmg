﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace BMGProject.Models
{
    [Table(name:"Movement")]
    public class MovementModel
    {
        [Column(name:"movement_id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Column(name: "account_id")]
        public int AccountID { get; set; }
        [Column(name: "movement_type")]
        public string MovementType { get; set; }
        [Column(name: "storage_date")]
        public DateTime StoregeDate { get; set; }
        [Column(name: "request_date")]
        public DateTime RequestDate { get; set; }
        [Column(name: "issuer_id")]
        public int IssuerID { get; set; }
        [Column(name: "share_price")]
        public double SharePrice { get; set; }
        [Column(name: "total_shares")]
        public double TotalShare { get; set; }
        [Column(name: "was_applied")]
        public bool WasApplied { get; set; }
        [Column(name: "error_code")]
        public int? ErrorCode { get; set; }

        [NotMapped]
        public IssuerModel MovementIssuer { get; set; }


        public MovementModel()
        {
            this.MovementType = String.Empty;
            this.RequestDate = DateTime.Now;
            this.WasApplied = true;
            this.ErrorCode = null;
            this.MovementIssuer = new IssuerModel();
        }

        public class Type
        {
            public const string BUY = "BUY";
            public const string SELL = "SELL";
        }
    }
}
