﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMGProject
{
    /// <summary>
    /// This class was created with the purpose to don't have magic elements
    /// </summary>
    public class ListOfVariablesName
    {
        public const string HOUR_CLOSE_MARKET = "HourCloseMarket";
        public const string HOUR_OPEN_MARKET = "HourOpenMarket";
    }
}
