﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BMGProject.Models;

namespace BMGProject.Data
{
    public class BMGProjectContext : DbContext
    {
        public BMGProjectContext(DbContextOptions<BMGProjectContext> options)
            : base(options)
        {
        }

        public DateTime GetServerDateTime()
        {
            return this.Account.Select(e => DateTime.Now).FirstOrDefault();
        }

        public DbSet<BMGProject.Models.MovementModel> Movement { get; set; }

        public DbSet<BMGProject.Models.AccountModel> Account { get; set; }

        public DbSet<BMGProject.Models.IssuerModel> Issuer { get; set; }
    }
}
